import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, ToastController, LoadingController, ModalController, AlertController, PopoverController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import { ViewDoc } from './view-doc/view-doc';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-upload-doc',
  templateUrl: 'upload-doc.html',
})
export class UploadDocPage implements OnInit {
  docType: any;
  docImge: boolean;
  islogin: any;
  docexp_date: any;
  vehData: any;
  docname: any;
  myVar: any;
  rcPresent: boolean;
  pucPresent: boolean;
  insurencePresent: boolean;
  licencePresent: boolean;
  permittPresent: boolean;
  fitmentPresent: boolean;
  _docTypeList: any[] = [];
  imgData: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public apiCall: ApiServiceProvider,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public event: Events
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.vehData = this.navParams.get("vehData");
    this.event.subscribe("reloaddoclist", () => {
      this._docTypeList = [];
      this.checkDevice();
    })
  }

  ngOnInit() {
    this.checkDevice();
  }

  _addDocument() {
    this.navCtrl.push("AddDocPage", { vehData: this.vehData });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadDocPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSelectChange(selectedValue: any) {
    console.log('Selected', selectedValue);
    this.docType = selectedValue;
  }

  brows() {
    this.docImge = true;
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  viewDocFunc(imgData) {
    // let that = this;
    console.log("imgData: ", imgData)
    if (imgData == undefined) {
      this.showToast('Data not found');
    } else {
      let modal = this.modalCtrl.create(ViewDoc, {
        param1: imgData
      });
      modal.present()
    }
  }

  checkDevice() {
    this.apiCall.startLoading();
    this.apiCall.getsingledevice(this.vehData.Device_ID)
      .subscribe(data => {
        this.apiCall.stopLoading();
        if (data.imageDoc.length > 0) {
          // console.log("imagedoc data: " + JSON.stringify(data.imageDoc))
          for (var i = 0; i < data.imageDoc.length; i++) {
            this._docTypeList.push({
              docname: data.imageDoc[i].doctype,
              expDate: data.imageDoc[i].docdate ? data.imageDoc[i].docdate : 'N/A',
              imageURL: data.imageDoc[i].image
            })
          }
        }
      },
        err => {
          console.log(err)
          this.apiCall.stopLoading();
        })
  }

  presentPopover(ev, data) {
    console.log("populated=> " + JSON.stringify(data))
    let that = this;
    let popover = this.popoverCtrl.create(DocPopoverPage,
      {
        vehData: that.vehData,
        imgData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => {
      this._docTypeList = [];
      this.checkDevice();
    })

    popover.present({
      ev: ev
    });
  }

}

@Component({
  template: `
    <ion-list>
      <ion-item class="text-palatino" (click)="editItem()">
        <ion-icon name="create"></ion-icon>&nbsp;&nbsp;Edit
      </ion-item>
      <ion-item class="text-san-francisco" (click)="deleteDoc()">
        <ion-icon name="trash"></ion-icon>&nbsp;&nbsp;Delete
      </ion-item>
      <ion-item class="text-seravek" (click)="shareItem()">
        <ion-icon name="share"></ion-icon>&nbsp;&nbsp;Share
      </ion-item>
      
    </ion-list>
  `
})
// <ion-item id="step1" class="text-seravek" (click)="upload()">
//        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
//        </ion-item>
export class DocPopoverPage implements OnInit {
  contentEle: any;
  textEle: any;
  vehData: any = {};
  islogin: any;
  imgData: any = {};
  link: string;

  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public socialSharing: SocialSharing
  ) {
    this.vehData = navParams.get("vehData");
    this.imgData = navParams.get("imgData");
    console.log("popover data=> ", this.imgData);
    var str = this.imgData.imageURL;
        var str1 = str.split('public/');
        this.link = this.apiCall.mainUrl + str1[1];

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + this.islogin);
  }

  ngOnInit() { }

  editItem() {
    console.log("edit")
  }

  deleteDoc() {
    let that = this;
    let alert = this.alertCtrl.create({
      message: 'Do you want to delete this Document?',
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          console.log(that.vehData.Device_ID)
          that.deleteDevice(that.vehData.Device_ID);
        }
      },
      {
        text: 'NO'
      }]
    });
    alert.present();
  }

  deleteDevice(d_id) {
    let that = this;
    var payload = {
      devId: d_id,
      doctype: that.imgData.docname
    }
    this.apiCall.startLoading().present();
    var baseUrl = this.apiCall.mainUrl + "devices/deleteDocuments";
    this.apiCall.urlpasseswithdata(baseUrl, payload)
      .subscribe(data => {
        this.apiCall.stopLoading();
        var DeletedDevice = data;
        console.log(DeletedDevice);

        let toast = this.toastCtrl.create({
          message: 'Document deleted successfully!',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.viewCtrl.dismiss();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  shareItem() {
    this.socialSharing.share(this.islogin.fn + " " + this.islogin.ln + " has shared the document with you.", "OneQlik- Vehicle Document", this.link, "");
  }

}
