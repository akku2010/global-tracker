import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaywiseReportPage } from './daywise-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    DaywiseReportPage,
  ],
  imports: [
    IonicPageModule.forChild(DaywiseReportPage),
    SelectSearchableModule
  ],
})
export class DaywiseReportPageModule {}
